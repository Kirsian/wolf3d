# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: dzabolot <dzabolot@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/03/06 17:09:40 by dzabolot          #+#    #+#              #
#    Updated: 2017/03/30 18:03:33 by dzabolot         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = wolf3D

IDIR = include/

VPATH = src:include

LIB = libft/libft.a

SRCS =	main.c				\
		constr.c			\
		wolf.c				\
		hooks.c				\
		additional.c		\
		hook_additional.c	\

F = -Wall -Wextra -Werror -I$(IDIR) -O3 -g

FLAGS = -lmlx -framework OpenGL -framework AppKit

BINS = $(SRCS:.c=.o)

all: $(NAME)

$(NAME): $(BINS)
	make -C libft/
	gcc -o $(NAME) $(BINS) $(F) $(FLAGS) $(LIB)

%.o: %.c
	gcc $(F) -c -o $@ $<

clean:
	make -C libft/ clean
	/bin/rm -f $(BINS)

fclean: clean
	make -C libft/ fclean
	/bin/rm -f $(NAME)

re: fclean all
