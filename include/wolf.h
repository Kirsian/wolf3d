/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wolf.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dzabolot <dzabolot@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/06 18:40:14 by dzabolot          #+#    #+#             */
/*   Updated: 2017/03/31 15:50:21 by dzabolot         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef WOLF_H
# define WOLF_H
# define W 1024
# define H 768
# define TW 1024
# define TH 1024

# include "stdlib.h"
# include "fcntl.h"
# include "math.h"
# include "../libft/libft.h"
# include "mlx.h"
# include <time.h>

typedef	struct		s_flag
{
	int				up;
	int				down;
	int				right;
	int				left;
}					t_flag;

typedef	struct		s_ltp
{
	int				width;
	int				height;
	void			*img;
}					t_ltp;

typedef	struct		s_wf
{
	void			*mlx;
	void			*win;
	void			*img;
	double			posx;
	double			posy;
	double			posxs;
	double			posys;
	double			dirx;
	double			olddirx;
	double			olddiry;
	double			diry;
	double			planex;
	double			planey;
	double			oldplanex;
	double			oldplaney;
	double			time;
	double			oldtime;
	double			camx;
	double			rpx;
	double			rpy;
	double			rdx;
	double			rdy;
	int				mapx;
	int				mapy;
	double			sidedistx;
	double			sidedisty;
	double			deltadistx;
	double			deltadisty;
	double			perpwalld;
	int				stepx;
	int				stepy;
	int				hit;
	int				side;
	int				lineheight;
	int				ds;
	int				ds_f;
	int				de;
	int				color;
	double			frametime;
	char			**wm;
	char			**wms1;
	t_flag			ev;
	double			ms;
	double			rots;
	int				texnum;
	double			wallx;
	int				texx;
	t_ltp			*texpack;
	int				rez_x;
	int				x;
	int				y;
	double			fxw;
	double			fyw;
	double			distw;
	double			distp;
	double			currd;
	double			weight;
	double			currfx;
	double			currfy;
	int				ftx;
	int				fty;
	int				menu_flag;
	int				evnet_f;
	int				game;
}					t_wf;

void				constr_wolf(t_wf *wolf);
void				loop(t_wf *w);
void				verline(int x, int ds, int de, int color, t_wf *w);
int					ft_hook(t_wf *w);
int					zaloop_hook(t_wf *w);
void				plain_txt(t_wf *map, int y_size, char *str, int color);
void				verline2(t_wf *w);
void				verline3(t_wf *w);
unsigned int		imggp(void *img, int x, int y);
void				imgpp(t_wf *map, int x, int y, int rgb);
int					exitx(t_wf *w);
double				deg2rad(double deg);
int					button_press(int keycode, t_wf *w);
int					button_release(int keycode, t_wf *w);
void				ft_menu(t_wf *w);
void				ft_hook_adittwo(t_wf *w);

#endif
