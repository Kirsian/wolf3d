/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dzabolot <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/30 13:09:17 by dzabolot          #+#    #+#             */
/*   Updated: 2016/11/30 13:09:17 by dzabolot         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

char	*ft_itoa(int n)
{
	char	*result;
	int		size;
	int		i;

	i = n;
	size = (n < 0) ? 2 : 1;
	while (i /= 10)
		size++;
	result = (char *)malloc(size + 1);
	if (!result)
		return (NULL);
	if (n < 0)
		result[0] = '-';
	result[size] = '\0';
	while (size--)
	{
		if (n >= 0)
			result[size] = (n % 10) + '0';
		if (n < 0)
			result[size] = '0' - (n % 10);
		n /= 10;
		if (n == 0)
			break ;
	}
	return (result);
}
