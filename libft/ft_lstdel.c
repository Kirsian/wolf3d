/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dzabolot <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/01 19:30:23 by dzabolot          #+#    #+#             */
/*   Updated: 2016/12/01 19:30:24 by dzabolot         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

void	ft_lstdel(t_list **alst, void (*del)(void *, size_t))
{
	t_list	*buff;
	t_list	*nextbuff;

	buff = *alst;
	while (buff)
	{
		del(buff->content, buff->content_size);
		nextbuff = buff->next;
		free(buff);
		buff = nextbuff;
	}
	(*alst) = NULL;
}
