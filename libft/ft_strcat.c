/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcat.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dzabolot <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/22 18:29:33 by dzabolot          #+#    #+#             */
/*   Updated: 2016/11/22 18:29:34 by dzabolot         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strcat(char *str1, const char *str2)
{
	int len;
	int i;

	len = (int)ft_strlen(str1);
	i = -1;
	while (str2[++i] != '\0')
	{
		str1[len] = str2[i];
		len++;
	}
	str1[len] = '\0';
	return (str1);
}
