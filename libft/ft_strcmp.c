/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dzabolot <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/22 16:44:28 by dzabolot          #+#    #+#             */
/*   Updated: 2016/11/22 16:44:30 by dzabolot         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_strcmp(char const *str, char const *str1)
{
	int i;

	i = 0;
	while (str[i] || str1[i])
	{
		if ((unsigned char)str[i] != (unsigned char)str1[i])
			return ((unsigned char)str[i] - (unsigned char)str1[i]);
		i++;
	}
	return (0);
}
