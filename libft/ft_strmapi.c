/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmapi.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dzabolot <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/28 19:59:36 by dzabolot          #+#    #+#             */
/*   Updated: 2016/11/28 19:59:36 by dzabolot         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

char	*ft_strmapi(char const *s, char (*f)(unsigned int, char))
{
	unsigned int	i;
	unsigned int	len;
	char			*new;

	new = NULL;
	if (s != 0)
	{
		i = -1;
		len = ft_strlen(s);
		if (!(new = malloc(sizeof(char) * len + 1)))
			return (NULL);
		while (++i < len)
		{
			new[i] = f(i, s[i]);
		}
		new[i] = '\0';
	}
	return (new);
}
