/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dzabolot <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/29 17:11:10 by dzabolot          #+#    #+#             */
/*   Updated: 2016/11/29 17:11:10 by dzabolot         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

static int		make_word(int p, char **result, char const *s, char c)
{
	int i;
	int lp;
	int len;

	lp = p;
	while (s[lp] != c)
		lp++;
	len = lp - p;
	*result = (char *)malloc(sizeof(char) * (len + 1));
	i = 0;
	while (i < len)
	{
		(*result)[i] = s[p + i];
		i++;
	}
	(*result)[i] = '\0';
	p = lp;
	return (p);
}

static int		cmp(char s, char c)
{
	if (s == '\0')
		return (1);
	return (s == c);
}

static int		count_size(const char *s, char c)
{
	int i;
	int count;

	i = 0;
	count = 0;
	while (s[i] != '\0')
	{
		if (cmp(s[i], c) == 0 && cmp(s[i + 1], c) == 1)
			count++;
		i++;
	}
	return (count);
}

char			**ft_strsplit(char const *s, char c)
{
	char	**result;
	int		len;
	size_t	pointer;
	int		count;

	if (!s)
		return (NULL);
	len = count_size(s, c);
	result = (char **)malloc(sizeof(char *) * (len + 1));
	if (!result)
		return (NULL);
	pointer = 0;
	count = 0;
	while (pointer < ft_strlen(s))
	{
		if (s[pointer] != c)
		{
			pointer = make_word(pointer, result + count, s, c);
			count++;
		}
		pointer++;
	}
	result[count] = NULL;
	return (result);
}
