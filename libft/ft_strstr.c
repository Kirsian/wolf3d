/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dzabolot <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/22 17:42:04 by dzabolot          #+#    #+#             */
/*   Updated: 2016/11/22 17:42:06 by dzabolot         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strstr(const char *str, const char *str1)
{
	int i;
	int j;

	i = 0;
	while (str[i] != '\0' || str1[i] != '\0')
	{
		j = 0;
		while (str[i + j] == str1[j])
		{
			if (str1[j + 1] == '\0')
				return ((char*)&str[i]);
			j++;
		}
		if (str[i] == '\0')
			break ;
		i++;
	}
	if (str1[0] == '\0')
		return ((char*)str);
	return (NULL);
}
