/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dzabolot <dzabolot@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/29 17:46:29 by dzabolot          #+#    #+#             */
/*   Updated: 2017/03/23 21:25:58 by dzabolot         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static	char		*ft_joinsp(char *str1, char *str2)
{
	char			*mas;

	if (str1 == NULL || str2 == NULL)
		return (NULL);
	mas = ft_strnew(ft_strlen(str1) + ft_strlen(str2) + 1);
	if (mas != NULL)
	{
		mas = ft_strcat(mas, str1);
		mas = ft_strcat(mas, str2);
		free(str1);
		free(str2);
		return (mas);
	}
	return (NULL);
}

t_lst				*creat_list(t_lst **lst, int fd)
{
	t_lst			**ptr;

	ptr = NULL;
	ptr = lst;
	while (*ptr)
	{
		if ((*ptr)->fd == fd)
			return (*(ptr));
		ptr = &(*ptr)->next;
	}
	if (*ptr == NULL)
	{
		*ptr = malloc(sizeof(**ptr));
		(*ptr)->s = NULL;
		(*ptr)->next = NULL;
		(*ptr)->fd = fd;
	}
	return (*(ptr));
}

int					from_list(t_lst *lst, char **line)
{
	char			*ptr;
	char			*f;

	if (!lst->s)
		return (0);
	if ((ptr = ft_strchr(lst->s, '\n')))
	{
		*line = ft_joinsp(*line, ft_strsub(lst->s, 0, ptr - lst->s));
		f = lst->s;
		lst->s = ft_strdup(lst->s + (ptr - lst->s) + 1);
		free(f);
		return (1);
	}
	else
	{
		*line = ft_joinsp(*line, ft_strdup(lst->s));
		ft_strdel(&lst->s);
		return (0);
	}
	return (0);
}

int					from_fd(t_lst *lst, char **line)
{
	int				eol;
	char			buffer[BUFF_SIZE + 1];
	char			*ptr;

	while ((eol = read(lst->fd, buffer, BUFF_SIZE)))
	{
		if (eol == -1)
			return (-1);
		buffer[eol] = '\0';
		if ((ptr = ft_strchr(buffer, '\n')))
		{
			*line = ft_joinsp(*line, ft_strsub(buffer, 0, ptr - buffer));
			lst->s = ft_strdup(ptr + 1);
			return (1);
		}
		else
		{
			*line = ft_joinsp(*line, buffer);
		}
	}
	if (eol == 0 && **line == '\0')
		return (0);
	return (1);
}

int					get_next_line(const int fd, char **line)
{
	static t_lst	*lst;
	t_lst			*ptr;

	if (!line || fd < 0)
		return (-1);
	*line = ft_strdup("");
	ptr = creat_list(&lst, fd);
	if (from_list(ptr, line))
		return (1);
	return (from_fd(ptr, line));
}
