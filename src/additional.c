/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   additional.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dzabolot <dzabolot@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/30 17:56:08 by dzabolot          #+#    #+#             */
/*   Updated: 2017/03/31 15:50:07 by dzabolot         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

void	plain_txt(t_wf *map, int y_size, char *str, int color)
{
	mlx_string_put(map->mlx, map->win, 10, y_size, color, str);
}

void	ft_hook_adittwo(t_wf *w)
{
	if (w->ev.down == 1)
	{
		if (w->wm[(int)(w->posx - w->dirx * w->ms - 0.4 *
			w->dirx)][(int)w->posy] <= '0')
			w->posx -= w->dirx * w->ms;
		if (w->wm[(int)w->posx][(int)(w->posy - w->diry * w->ms - 0.4 *
			w->diry)] <= '0')
			w->posy -= w->diry * w->ms;
	}
}
