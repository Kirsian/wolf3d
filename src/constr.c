/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   constr.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dzabolot <dzabolot@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/07 17:23:57 by dzabolot          #+#    #+#             */
/*   Updated: 2017/03/31 16:08:53 by dzabolot         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

void		make_map(char *str, char *map)
{
	char	**tab;
	int		i;

	tab = ft_strsplit(str, ',');
	i = 0;
	while (tab[i] != NULL)
	{
		map[i] = tab[i][0];
		ft_strdel(&tab[i]);
		i++;
	}
	ft_strdel(&tab[i]);
	free(tab);
	map[24] = '\0';
}

void		upload_texture(t_wf *wolf)
{
	wolf->texpack = (t_ltp *)malloc(sizeof(t_ltp) * 12);
	wolf->texpack[0].img = mlx_xpm_file_to_image(wolf->mlx, "t/1.xpm",
					&wolf->texpack[0].width, &wolf->texpack[0].height);
	wolf->texpack[1].img = mlx_xpm_file_to_image(wolf->mlx, "t/2.xpm",
					&wolf->texpack[1].width, &wolf->texpack[1].height);
	wolf->texpack[2].img = mlx_xpm_file_to_image(wolf->mlx, "t/3.xpm",
					&wolf->texpack[2].width, &wolf->texpack[2].height);
	wolf->texpack[3].img = mlx_xpm_file_to_image(wolf->mlx, "t/4.xpm",
					&wolf->texpack[3].width, &wolf->texpack[3].height);
	wolf->texpack[4].img = mlx_xpm_file_to_image(wolf->mlx, "t/5.xpm",
					&wolf->texpack[4].width, &wolf->texpack[4].height);
	wolf->texpack[5].img = mlx_xpm_file_to_image(wolf->mlx, "t/6.xpm",
					&wolf->texpack[5].width, &wolf->texpack[5].height);
	wolf->texpack[6].img = mlx_xpm_file_to_image(wolf->mlx, "t/7.xpm",
					&wolf->texpack[6].width, &wolf->texpack[6].height);
	wolf->texpack[7].img = mlx_xpm_file_to_image(wolf->mlx, "t/8.xpm",
					&wolf->texpack[7].width, &wolf->texpack[7].height);
	wolf->texpack[8].img = mlx_xpm_file_to_image(wolf->mlx, "t/new_game.xpm",
					&wolf->texpack[8].width, &wolf->texpack[8].height);
	wolf->texpack[9].img = mlx_xpm_file_to_image(wolf->mlx, "t/control.xpm",
					&wolf->texpack[9].width, &wolf->texpack[9].height);
	wolf->texpack[10].img = mlx_xpm_file_to_image(wolf->mlx, "t/exit.xpm",
					&wolf->texpack[10].width, &wolf->texpack[10].height);
	wolf->texpack[11].img = mlx_xpm_file_to_image(wolf->mlx, "t/control_in.xpm",
					&wolf->texpack[11].width, &wolf->texpack[11].height);
}

void		constr_wolf_adit(t_wf *wolf)
{
	wolf->posx = 1.5;
	wolf->posy = 1.5;
	wolf->dirx = -1;
	wolf->diry = 0;
	wolf->planex = 0;
	wolf->planey = 0.66;
	wolf->time = 0;
	wolf->oldtime = 0;
	wolf->ev.up = 0;
	wolf->ev.down = 0;
	wolf->ev.right = 0;
	wolf->ev.left = 0;
	wolf->menu_flag = 0;
	wolf->evnet_f = -1;
	wolf->game = 0;
	upload_texture(wolf);
}

void		constr_wolf(t_wf *wolf)
{
	char	*buf;
	int		i;
	int		fd;

	i = 0;
	fd = open("map/file", O_RDONLY);
	wolf->wm = (char **)malloc(sizeof(char *) * 25);
	while (get_next_line(fd, &buf) > 0)
	{
		wolf->wm[i] = (char *)malloc(sizeof(char) * 25);
		make_map(buf, wolf->wm[i]);
		i++;
		ft_strdel(&buf);
	}
	wolf->wm[24] = NULL;
	close(fd);
	wolf->wms1 = wolf->wm;
	ft_strdel(&buf);
	free(buf);
	i = 0;
	wolf->mlx = mlx_init();
	wolf->win = mlx_new_window(wolf->mlx, W, H, "wolf3D");
	wolf->img = mlx_new_image(wolf->mlx, W, H);
	constr_wolf_adit(wolf);
}
