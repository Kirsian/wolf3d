/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hook_additional.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dzabolot <dzabolot@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/30 17:57:55 by dzabolot          #+#    #+#             */
/*   Updated: 2017/03/31 16:10:00 by dzabolot         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

int		exitx(t_wf *w)
{
	w = NULL;
	exit(1);
	return (0);
}

double	deg2rad(double deg)
{
	return (M_PI / 180 * deg);
}

int		button_press(int keycode, t_wf *w)
{
	if (keycode == 126)
		w->ev.up = 1;
	if (keycode == 123)
		w->ev.left = 1;
	if (keycode == 125)
		w->ev.down = 1;
	if (keycode == 124)
		w->ev.right = 1;
	if (keycode == 53)
	{
		w->evnet_f = -1;
		w->menu_flag = 0;
	}
	if (keycode == 12)
		w->evnet_f = 2;
	if (keycode == 31)
	{
		if (w->wm[(int)(w->dirx + w->posx)][(int)(w->diry + w->posy)] == '6')
			w->wm[(int)(w->dirx + w->posx)][(int)(w->diry + w->posy)] = '-';
		else if (w->wm[(int)(w->dirx + w->posx)][(int)(w->diry + w->posy)] ==
				'-')
			w->wm[(int)(w->dirx + w->posx)][(int)(w->diry + w->posy)] = '6';
	}
	return (0);
}

int		button_release(int keycode, t_wf *w)
{
	if (keycode == 126)
		w->ev.up = 0;
	if (keycode == 123)
		w->ev.left = 0;
	if (keycode == 125)
		w->ev.down = 0;
	if (keycode == 124)
		w->ev.right = 0;
	return (0);
}

void	ft_menu(t_wf *w)
{
	w->img = w->texpack[w->menu_flag + 8].img;
	mlx_put_image_to_window(w->mlx, w->win, w->img, 0, 0);
}
