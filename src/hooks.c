/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hooks.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dzabolot <dzabolot@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/09 18:59:10 by dzabolot          #+#    #+#             */
/*   Updated: 2017/03/31 16:38:56 by dzabolot         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

void	button_press_menu_adit(int keycode, t_wf *w)
{
	if (w->menu_flag == 1 && keycode == 36)
		w->evnet_f = 1;
	if (w->evnet_f == 1 && keycode == 53)
		w->evnet_f = -1;
	if ((w->menu_flag == 2 && keycode == 36) || keycode == 12)
		w->evnet_f = 2;
	if (keycode == 53 && w->game == 1)
		w->evnet_f = 0;
}

int		button_press_menu(int keycode, t_wf *w)
{
	if (w->menu_flag > 5)
		return (0);
	if (keycode == 126 && w->menu_flag > 0 && w->evnet_f != 1)
		w->menu_flag--;
	if (keycode == 125 && w->menu_flag < 2 && w->evnet_f != 1)
		w->menu_flag++;
	if (w->menu_flag == 0 && keycode == 36)
		w->evnet_f = 0;
	if (w->menu_flag == 0 && keycode == 36 && w->game == 1)
	{
		w->posx = 1.5;
		w->posy = 1.5;
		w->wm = w->wms1;
		w->dirx = -1;
		w->diry = 0;
		w->planex = 0;
		w->planey = 0.66;
		w->wm[6][6] = '6';
		w->wm[6][10] = '6';
		w->wm[18][8] = '6';
	}
	button_press_menu_adit(keycode, w);
	return (0);
}

void	ft_hook_adit(t_wf *w)
{
	ft_hook_adittwo(w);
	if (w->ev.right == 1)
	{
		w->olddirx = w->dirx;
		w->dirx = w->dirx * cos(-w->rots) - w->diry * sin(-w->rots);
		w->diry = w->olddirx * sin(-w->rots) + w->diry * cos(-w->rots);
		w->oldplanex = w->planex;
		w->planex = w->planex * cos(-w->rots) - w->planey * sin(-w->rots);
		w->planey = w->oldplanex * sin(-w->rots) + w->planey * cos(-w->rots);
	}
	if (w->ev.left == 1)
	{
		w->olddirx = w->dirx;
		w->dirx = w->dirx * cos(w->rots) - w->diry * sin(w->rots);
		w->diry = w->olddirx * sin(w->rots) + w->diry * cos(w->rots);
		w->oldplanex = w->planex;
		w->planex = w->planex * cos(w->rots) - w->planey * sin(w->rots);
		w->planey = w->oldplanex * sin(w->rots) + w->planey * cos(w->rots);
	}
}

int		ft_hook(t_wf *w)
{
	char	*str;

	w->img = mlx_new_image(w->mlx, W, H);
	loop(w);
	w->oldtime = w->time;
	w->time = clock();
	w->frametime = (w->time - w->oldtime) / CLOCKS_PER_SEC;
	str = ft_itoa(1. / w->frametime);
	w->ms = w->frametime * 5;
	w->rots = w->frametime * 3.0;
	if (w->ev.up == 1)
	{
		if (w->wm[(int)(w->posx + w->dirx * w->ms + 0.4 *
			w->dirx)][(int)w->posy] <= '0')
			w->posx += w->dirx * w->ms;
		if (w->wm[(int)w->posx][(int)(w->posy + w->diry * w->ms + 0.4 *
			w->diry)] <= '0')
			w->posy += w->diry * w->ms;
	}
	ft_hook_adit(w);
	mlx_put_image_to_window(w->mlx, w->win, w->img, 0, 0);
	mlx_destroy_image(w->mlx, w->img);
	plain_txt(w, 20, str, 0x00FF00);
	free(str);
	return (0);
}

int		zaloop_hook(t_wf *w)
{
	mlx_hook(w->win, 17, 1L << 17, exitx, w);
	if (w->evnet_f == -1)
	{
		ft_menu(w);
		mlx_hook(w->win, 2, 1, button_press_menu, w);
	}
	if (w->evnet_f == 1)
	{
		mlx_put_image_to_window(w->mlx, w->win, w->texpack[11].img, 0, 0);
		mlx_hook(w->win, 2, 1, button_press_menu, w);
	}
	if (w->evnet_f == 2)
		exit(1);
	if (w->evnet_f == 0)
	{
		w->game = 1;
		ft_hook(w);
		mlx_hook(w->win, 2, 1, button_press, w);
		mlx_hook(w->win, 3, 2, button_release, w);
	}
	return (0);
}
