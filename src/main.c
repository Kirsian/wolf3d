/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dzabolot <dzabolot@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/06 17:50:07 by dzabolot          #+#    #+#             */
/*   Updated: 2017/03/31 16:11:44 by dzabolot         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

unsigned int	imggp(void *img, int x, int y)
{
	int				bpp;
	int				si;
	int				e;
	char			*im;
	unsigned int	res;

	res = 0;
	im = mlx_get_data_addr(img, &bpp, &si, &e);
	if (x >= 0 && x < TH && y >= 0 && y < TW)
	{
		ft_memcpy((void *)&res, (void *)((im + y * TW *
					(bpp / 8) + x * (bpp / 8))), 4);
	}
	return (res);
}

void			imgpp(t_wf *map, int x, int y, int rgb)
{
	int				bpp;
	int				si;
	int				e;
	char			*im;
	unsigned int	tmp;

	im = mlx_get_data_addr(map->img, &bpp, &si, &e);
	tmp = mlx_get_color_value(map->mlx, rgb);
	if (x >= 0 && x < W && y >= 0 && y < H)
	{
		ft_memcpy((void *)((im + y * W *
							(bpp / 8) + x * (bpp / 8))), (void *)&tmp, 4);
	}
}

void			verline2(t_wf *w)
{
	int d;
	int texy;
	int color;

	while (w->ds <= w->de)
	{
		d = (w->ds << 1) - H + w->lineheight;
		texy = ((d * TH) / w->lineheight) >> 1;
		color = (int)imggp(w->texpack[w->texnum].img, w->texx, texy);
		if (w->side == 1)
			color = (color >> 1) & 8355711;
		w->ds++;
		imgpp(w, w->x, w->ds, color);
	}
}

void			verline3(t_wf *w)
{
	int colorf;
	int colorc;

	while (w->y < H)
	{
		w->currd = H / (2.0 * w->y - H);
		w->weight = (w->currd - w->distp) / (w->distw - w->distp);
		w->currfx = w->weight * w->fxw + (1.0 - w->weight) * w->posx;
		w->currfy = w->weight * w->fyw + (1.0 - w->weight) * w->posy;
		w->ftx = (int)(w->currfx * TW) % TW;
		w->fty = (int)(w->currfy * TH) % TH;
		colorf = (int)imggp(w->texpack[7].img, w->ftx, w->fty);
		colorc = (int)imggp(w->texpack[6].img, w->ftx, w->fty);
		w->y++;
		imgpp(w, w->x, w->y, colorf);
		imgpp(w, w->x, H - w->y, colorc);
	}
}

int				main(void)
{
	t_wf	wolf;

	constr_wolf(&wolf);
	mlx_loop_hook(wolf.mlx, zaloop_hook, &wolf);
	mlx_loop(wolf.mlx);
	return (0);
}
