/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wolf.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dzabolot <dzabolot@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/07 17:40:01 by dzabolot          #+#    #+#             */
/*   Updated: 2017/03/31 16:05:39 by dzabolot         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

void	loop_one(t_wf *w)
{
	if (w->rdx < 0)
	{
		w->stepx = -1;
		w->sidedistx = (w->rpx - w->mapx) * w->deltadistx;
	}
	else
	{
		w->stepx = 1;
		w->sidedistx = (w->mapx + 1.0 - w->rpx) * w->deltadistx;
	}
	if (w->rdy < 0)
	{
		w->stepy = -1;
		w->sidedisty = (w->rpy - w->mapy) * w->deltadisty;
	}
	else
	{
		w->stepy = 1;
		w->sidedisty = (w->mapy + 1.0 - w->rpy) * w->deltadisty;
	}
}

void	loop_two(t_wf *w)
{
	while (w->hit == 0)
	{
		if (w->sidedistx < w->sidedisty)
		{
			w->sidedistx += w->deltadistx;
			w->mapx += w->stepx;
			w->side = 0;
		}
		else
		{
			w->sidedisty += w->deltadisty;
			w->mapy += w->stepy;
			w->side = 1;
		}
		if (w->wm[w->mapx][w->mapy] > '0')
			w->hit = 1;
	}
	if (w->side == 0)
		w->perpwalld = (w->mapx - w->rpx + (1. - w->stepx) / 2.) / w->rdx;
	else
		w->perpwalld = (w->mapy - w->rpy + (1 - w->stepy) / 2) / w->rdy;
	w->lineheight = (int)(H / w->perpwalld);
	w->ds = -w->lineheight / 2 + H / 2;
	if (w->ds < 0)
		w->ds = 0;
}

void	loop_three(t_wf *w)
{
	w->de = w->lineheight / 2 + H / 2;
	if (w->de >= H)
		w->de = H - 1;
	w->texnum = w->wm[w->mapx][w->mapy] - '1';
	if (w->side == 0)
		w->wallx = w->rpy + w->perpwalld * w->rdy;
	else
		w->wallx = w->rpx + w->perpwalld * w->rdx;
	w->wallx -= floor(w->wallx);
	w->texx = (int)(w->wallx * (double)(TW));
	if (w->side == 0 && w->rdx > 0)
		w->texx = TW - w->texx - 1;
	if (w->side == 1 && w->rdy < 0)
		w->texx = TW - w->texx - 1;
	verline2(w);
}

void	loop_four(t_wf *w)
{
	if (w->side == 0 && w->rdx > 0)
	{
		w->fxw = w->mapx;
		w->fyw = w->mapy + w->wallx;
	}
	else if (w->side == 0 && w->rdx < 0)
	{
		w->fxw = w->mapx + 1.0;
		w->fyw = w->mapy + w->wallx;
	}
	else if (w->side == 1 && w->rdy > 0)
	{
		w->fxw = w->mapx + w->wallx;
		w->fyw = w->mapy;
	}
	else
	{
		w->fxw = w->mapx + w->wallx;
		w->fyw = w->mapy + 1.0;
	}
	w->distw = w->perpwalld;
	w->distp = 0.0;
	if (w->de < 0)
		w->de = H;
	w->y = w->de;
}

void	loop(t_wf *w)
{
	w->x = 0;
	while (w->x < W)
	{
		w->camx = 2 * w->x / (double)W - 1;
		w->rpx = w->posx;
		w->rpy = w->posy;
		w->rdx = w->dirx + w->planex * w->camx;
		w->rdy = w->diry + w->planey * w->camx;
		w->mapx = (int)w->rpx;
		w->mapy = (int)w->rpy;
		w->deltadistx = sqrt(1 + (w->rdy * w->rdy) / (w->rdx * w->rdx));
		w->deltadisty = sqrt(1 + (w->rdx * w->rdx) / (w->rdy * w->rdy));
		w->hit = 0;
		loop_one(w);
		loop_two(w);
		loop_three(w);
		loop_four(w);
		verline3(w);
		w->x++;
	}
}
